function values(testObject){
    if(typeof testObject !== 'object'||testObject===null ||testObject=== undefined){
        return "testObject is undefined or null";
    }
    const valuesOfObject=[];
    for(let key in testObject){
        valuesOfObject.push(testObject[key]);
    }
    return valuesOfObject;
}

module.exports=values;