function defaults(testObject,defaultProps){
    if(testObject===null ||testObject=== undefined){
        return "testObject is undefined or null";
    }
    if (defaultProps===null || defaultProps === undefined) {
        return "defaultProps is undefined or null";
    }
    for(let key in testObject){
        if(testObject[key]===undefined){
            testObject[key]=defaultProps[key];
        }
    }
    return testObject;
}


module.exports=defaults;