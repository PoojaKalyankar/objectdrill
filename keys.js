function keys(testObject){
    if(typeof testObject!=='object'|| testObject===null ||testObject=== undefined){
        return "testObject is undefined or null";
    }
    const keysOnly=[];
    for(let key in testObject){
        keysOnly.push(String(key));
    }
    return keysOnly;
}

module.exports=keys;