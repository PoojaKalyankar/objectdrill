function mapObject(testObject,cb){
    if(typeof testObject !=='object'|| testObject===null ||testObject=== undefined){
        return "testObject is undefined or null";
    }
    let newObject={};
    for(let key in testObject){
        newObject[key]=cb(key,testObject);
    }
    return newObject;
}
module.exports=mapObject;