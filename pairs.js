function pairs(testObject){
  if(typeof testObject !== 'object' || testObject===null ||testObject=== undefined){
    return "testObject is undefined or null";
  }

    const list=[];
    for(let key in testObject){
      let pairArray=[];
      pairArray.push(`${key} ,${testObject[key]}`);
      list.push(pairArray);
    }
  return list;
}

module.exports=pairs;