function invert(testObject){
    if(typeof testObject !=='object' || testObject===null ||testObject=== undefined){
        return "testObject is undefined or null";
    }
    let newObject={};
    for(let key in testObject){
        newObject[testObject[key]]=key;
    }
    return newObject;
}

module.exports=invert;